# Olympics Expo
## Overview 
Olympics Expo is a web application that  provides Olympics related information, including  details of medalists and medal count rankings. A trivia quiz is also implemented to test user’s knowledge about Olympics. On our site, users can create an account to keep track of their quiz performance and view the leaderboard of high scores achieved by other users. Additionally, a bing search bar is provided for user to search more information.

## Motivation
This project is motivated by the popularity of summer olympics games. In addition to watching the games we are also interested in learning the history of Olympics and how rankings of medal count would change if more information of countries are considered, such as GDP and population. To stimulate user’s interest while they learn more about Olympics, a trivia game is designed. This web application is designed for students to learn more about Olympics in a fun way. 

## To start the App
npm install

npm start

## Modules
* aws-sdk: AWS module for connecting to DynamoDB
* bcrypt: encrypt user password with bcrypt
* body-parser: parse incoming request bodies
* connect-ensure-login: keep user’s login status
* cookie-parser: parse Cookie header
* debug: small debugging utility
* ejs: render embedded JavaScript templates
* express: web development framework
* express-session: create a session middleware
* method-override: override HTTP verbs
* morgan: HTTP request logger
* mysql: MySQL database client
* passport: provide authentication
* passport-local: authentication strategy for passport.js
* serve-favicon: serve browser the favicon

## Technical Specification
* Ruby: fetching and formatting data from HTML and CSV
* Java: data cleaning and HTML web pages parsing
* Node.js & Express: Javascript runtime environment and web framework
* MySQL: RDBMS to store Olympic and other complementary data
* Amazon EC2: hosting the web server
* DynamoDB: NoSQL database to store user information
* Bing search API: online search
* Facebook API: Facebook login information 
