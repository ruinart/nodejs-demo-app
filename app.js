var express = require('express');
var results = require('./routes/results');
var update = require('./routes/update');
var quiz = require('./routes/quiz');
var rank = require('./routes/rank');
var search = require('./routes/search');
var board = require("./routes/board");
var path = require('path');
var users = require("./routes/users");
var bcrypt = require('bcrypt');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var app = express();

var passport = require('passport');
var Strategy = require('passport-local').Strategy;
app.use(favicon(__dirname + '/public/favicon.png'));
passport.use(new Strategy( { usernameField: 'email' },
  function(email, password, cb) {
    users.findByUsername(email, function(err, user) {
      if (err) { return cb(err); }
      if (!user) { return cb(null, false); }
      bcrypt.compare(password, user.password, function(err, check) {
        if (err) { console.error("Unable to compare hash. Error JSON:", JSON.stringify(err, null, 2)); }
        if (check != true) { return cb(null, false); }
        return cb(null, user);
      });
    });
  }));

passport.serializeUser(function(user, cb) {
  cb(null, user.id);
});

passport.deserializeUser(function(id, cb) {
  users.findById(id, function (err, user) {
    if (err) { return cb(err); }
    cb(null, user);
  });
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
app.use(logger('dev'));
app.use(require('method-override')());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(require('express-session')({ secret: 'keyboard cat', resave: false, saveUninitialized: false }));
app.use(passport.initialize());
app.use(passport.session());

app.get('/', function(req, res){
  res.render('index', {title: "The World of Olympics", user: req.user});
});
app.get('/results', results.show);
// used for table updating
app.get('/update', update.update);
// used for quiz
app.get('/quiz', quiz.quiz);
app.get('/rank', rank.show);
app.get('/search', search.show);
app.get('/about', function(req, res){
    res.render('about', {title: "About Us", user: req.user});
});
app.get('/board', board.show);
app.get('/login',
  function(req, res){
    res.render('login', { title: 'Log in', user: req.user });
  });
app.get('/signup',
  function(req, res){
    res.render('signup', { title: 'Sign up', user: req.user });
  });
app.post('/signup', users.signUp);
app.post('/login', 
  passport.authenticate('local', { failureRedirect: '/login' }),
  function(req, res) {
    res.redirect('/profile');
  });
app.get('/logout',
  function(req, res){
    req.logout();
    res.redirect('/');
  });

app.get('/profile',
  require('connect-ensure-login').ensureLoggedIn(),
  function(req, res){
    res.render('profile', { title: 'Profile', user: req.user });
  });

app.post('/score', users.uploadScore);

app.use(express.static(path.join(__dirname, 'public')));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error', {title: "404 Error"});
});

module.exports = app;
