$(document).ready(function() {
      // update edition when nation changed.
      $('#search').click(function() {
        
        var query = document.getElementById("bing-search").value;
        var params = {
            // Request parameters
            "q": query,
            "count": "20",
            "offset": "0",
            "mkt": "en-us",
            "safeSearch": "Moderate",
        };
      
        $.ajax({
            url: "https://api.cognitive.microsoft.com/bing/v5.0/search?" + $.param(params),
            beforeSend: function(xhrObj){
                // Request headers
                xhrObj.setRequestHeader("Ocp-Apim-Subscription-Key","4aa450b17e3748e695629f579265c851");
            },
            type: "GET",
            // Request body
            
        })
        .done(function(data) {
            var results = "";
            for (var i = 0; i < data.webPages.value.length; i++) {
              if (data.webPages.value[i].displayUrl.substring(0, 4) === "http") {
              results += "<a href='" + data.webPages.value[i].displayUrl + "'><u>" + data.webPages.value[i].name + "</u></a>" + "<p>" + data.webPages.value[i].snippet + "</p>";
              }
              else {
                results += "<a href='http://" + data.webPages.value[i].displayUrl + "'><u>" + data.webPages.value[i].name + "</u></a>" + "<p>" + data.webPages.value[i].snippet + "</p>";
              }
            }
        $('#bing-results .modal-content .modal-body').html(results);
        var modal = document.getElementById('bing-results');
        var span = document.getElementsByClassName("close")[0];
        modal.style.display = "block";
// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
};

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
};
            
        })
        .fail(function() {
            alert("Please search something!");
        });
      });
});