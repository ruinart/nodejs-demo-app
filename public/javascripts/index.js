$(document).ready(function() {
      // update edition when nation changed.
      $('#nat').change(function() {
        $.get("/update?nation="+$('#nat').val(), function(data, status) {
          // $('#div1').text(data);
          $('#edi').html(data);
          var edition = data.split('</option>')[0].substr(68,4);
          $.get("/update?nation="+$('#nat').val()+"&edition="+edition, function(data2, status) {
            $('#dis').html(data2);
          });
        });
      });
      // update discipline when edition changed.
      $('#edi').change(function() {
        $.get("/update?nation="+$('#nat').val()+"&edition="+$('#edi').val(), function(data, status) {
          $('#dis').html(data);
        });
      });
      $('#search').click(function() {
        var query = document.getElementById("bing-search").value;
        var params = {
            // Request parameters
            "q": query,
            "count": "20",
            "offset": "0",
            "mkt": "en-us",
            "safeSearch": "Moderate",
        };
      
        $.ajax({
            url: "https://api.cognitive.microsoft.com/bing/v5.0/search?" + $.param(params),
            beforeSend: function(xhrObj){
                // Request headers
                xhrObj.setRequestHeader("Ocp-Apim-Subscription-Key","4aa450b17e3748e695629f579265c851");
            },
            type: "GET",
            // Request body
            
        })
        .done(function(data) {
            var results = "";
            for (var i = 0; i < data.webPages.value.length; i++) {
              if (data.webPages.value[i].displayUrl.substring(0, 4) === "http") {
              results += "<a href='" + data.webPages.value[i].displayUrl + "'>" + data.webPages.value[i].name + "</a>" + "<p>" + data.webPages.value[i].snippet + "</p>";
              }
              else {
                results += "<a href='http://" + data.webPages.value[i].displayUrl + "'>" + data.webPages.value[i].name + "</a>" + "<p>" + data.webPages.value[i].snippet + "</p>";
              }
            }
            $('#searchResults').html(results);
        })
        .fail(function() {
            alert("Please search something!");
        });
      });
});