$(function(){
    var loading = $('#loadbar').hide();
    $(document)
    .ajaxStart(function () {
        loading.show();
    }).ajaxStop(function () {
    	loading.hide();
    });

    $correct = 0;
    $current = 1;
    $max = 10;
    $("#progress-bar").text(String($current) +"/"+ String($max));
    var value = String($current * 1.0 / $max * 100);
    $("#progress-bar").attr("style","width:"+value+"%");
    $("#progress-bar").attr("aria-valuenow",value);

    $("label.btn").on('click',function () {
    	var choice = $(this).find('input:radio').val();     
        // request answer.
        $.ajax({
            async: false,
            url: "/quiz?answer=1",
            type: "GET",
            success: function (data) {
                if (data.length > 100) {
                    window.location.href = "/quiz";
                } else {

                    if (data == choice) {
                        $('#image').attr("src","/images/right.png");
                        $("#response").text("Answer: "+data);
                        $('#response').css({ 'color': 'green'});
                        $correct += 1;
                    } else {
                        $('#image').attr("src","/images/wrong.png");
                        $("#response").text("Answer: "+data);
                        $('#response').css({ 'color': 'red'});
                    }
                    if ($current == $max) {
                        $("#next").html("Leaderboard <span class=\"glyphicon glyphicon-king\"></span>");
                        $("#next").hide();
                        // send score to the server
                        $.ajax({
                            async: false,
                            url: "/score",
                            type: "POST",
                            data: {score: $correct}
                        })
             
                        setTimeout(function(){
                            $("#evaluationModal").modal('show');
                            var output = "";
                            var output_title = "";
                            if ($correct >= $max*0.6) {
                                output = "Congratulation! You are an expert of Olympics!";
                            } else if ($correct >= $max*0.3){
                                output = "Not bad! Try next time!"
                            } else {
                                output = "Oops! Learn more on our website!"
                            }
                            $("#congrats").text(output);
                            $("#evaluate").text(String($correct)+" right answers out of "+String($max)+" possible.");
                            $("#next").show();
                        }, 2000);
                    }
                }
            },
            error: function (xhr, status) {
                // alert(status);
            }
        });

        $('#loadbar').show();
        $('#quiz').fadeOut();
 
    });

    $("#next").on('click', function() {
        if ($current < $max) {
            $current += 1;
            $("#progress-bar").text(String($current) +"/"+ String($max));
            var value = String($current * 1.0 / $max * 100);
            $("#progress-bar").attr("style","width:"+value+"%");
            $("#progress-bar").attr("aria-valuenow",value);

            $.ajax({
                async: false,
                url: "/quiz?next=1",
                type: "GET",
                success: function (data) {
                    var question_info = JSON.parse(JSON.stringify(data));
                    $("#question").html("<span class=\"label label-warning\" id=\"qid\">"+$current+"</span> " + question_info.questions);
                
                    for (var i = 0; i < question_info.choices.length; i++) {
                        var temp_choice = question_info.choices[i].choice;
                        $('#choice'+String(i)).html("<span class=\"btn-label\"><i class=\"glyphicon glyphicon-chevron-right\"></i></span> \
                            <input type=\"radio\" name=\"q_answer\" value=\""+temp_choice+"\">"+temp_choice);
                    }
                },
                error: function (xhr, status) {
                    alert(status);
                }
            });

            setTimeout(function(){
                $('#quiz').show();
                $('#loadbar').fadeOut();
            /* something else */
            }, 100);
        } else {
            // url should be changed to leaderboard
            window.location.href = "/board";
        }
    });
});	
