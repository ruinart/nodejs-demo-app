exports.show = function(req, res) {
    var AWS = require("aws-sdk");
    AWS.config.update({
      region: "us-east-1",
      endpoint: "dynamodb.us-east-1.amazonaws.com"
    });
    var docClient = new AWS.DynamoDB.DocumentClient();
    var scanParams = {
      TableName : "Users",
      ProjectionExpression: "#id, email, scores",
      ExpressionAttributeNames: { "#id": "id" }
    };
    docClient.scan(scanParams, function(err, data) {
    var board = [];
    if (err) {
        console.error("Unable to scan the table. Error JSON:", JSON.stringify(err, null, 2));
    } else if (data.Items[0]) {
        data.Items.forEach(function(item) {
            var email = item.email;
            if (item.scores[0]) {
            item.scores.forEach(function(score) {
                board.push([email, score[0], score[1]]);
            });
            }
      });
    }
    board.sort(function(a, b){return b[1]-a[1]});
    if (board.length > 10) {
        board = board.slice(0, 10);
    }
    res.render('board', {title: "Leaderboard", user: req.user, board: board});
  });
};