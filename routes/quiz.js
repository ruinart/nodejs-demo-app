// TODO
var mysql = require('mysql');
var connection = mysql.createConnection({
  host     : 'dbolympic.cajidywjgjlk.us-east-1.rds.amazonaws.com',
  user     : 'guest1',
  password : '12345678',
  database : 'olym'
});

var answers = "";

// Generate the answer based on the question No.
function generate_answer(req, res) {
	if (answers.length > 0) {
		var data = answers;
		res.writeHead(200, {'Content-Type': 'text/plain','Content-Length':data.length});
		res.write(data);
		res.end();
	} else {
		generate_quiz(req, res);
	}
}

// Generate the quiz
function generate_quiz(req, res) {
	var questions = "";
	var choices = "";
	answers = "";
	// generate a random integer 0,1,2,3,4 to indicate 4 question types
	var type = Math.floor(Math.random() * 5);
	// var type = 4;
	var question_info = [];
	question_type(type, function(returnValue) {
		// 0: question, 1: choices, 2: query
		if (returnValue.length > 0) {
			question_info = returnValue;
			questions = question_info[0];
			choices = question_info[1];
			answers = question_info[2];
			// output the result
			output_mywork(req, res, [questions, choices, answers]);
		} else {
			generate_quiz(req, res);
		}
	});	
}

// generate the question based on the type
function question_type(type, callback) {
	if (type == 0) {
		// return question1
		question1(function(returnValue) {
			callback(returnValue);
		});
	} else if (type == 1) {
		// return question2
		question2(function(returnValue) {
			callback(returnValue);
		});
	} else if (type == 2) {
		// return question3
		question3(function(returnValue) {
			callback(returnValue);
		});
	} else if (type == 3) {
		// return question4
		question4(function(returnValue) {
			callback(returnValue);
		});
	} else {
		// return question5
		question5(function(returnValue) {
			callback(returnValue);
		});	
	}
}

// Question 1: what is the nationality of an athlete?
function question1(callback) {
	// Pick one element randomly that used in question.
	// e.g. Who is a table tennis player.
	// So we need query the table and get table tennis back first.
	// For some questions, mutiple elements might be necessary.
	// e.g. Who won the gold medal as a table tennis player in 2008.
	var element_query = "SELECT aname AS element \
						FROM Medal \
						WHERE mtype = 'Gold' \
						GROUP BY aname \
						HAVING count(*) >3 \
						ORDER BY RAND() LIMIT 1;";
	connection.query(element_query, function(err, results, fields) {
		if (err) {
			console.log(err);
			callback([]);
		} else {
			// Get the result
			var element = JSON.parse(JSON.stringify(results))[0].element;
			if (typeof element == "undefined") {
				console.log("question1 no element.");
				callback([]);
			}
			var element1 = transformName(element);
			// Given the elements above, create a question.
			var question = "What is the nationality of "+ element1 +"?";
			// Generate a query that will return a right answer back.
			// This query should be used (connection.query...) in function generate_quiz.
			var query = "SELECT cname AS answer \
						FROM Athlete inner join Nation using (noc) \
						where aname = '"+element+"';";
			connection.query(query, function(err, results, fields) {
				if (err) {
					console.log(err);
					callback([]);
				} else {
					// Get the answer back
					var answer = JSON.parse(JSON.stringify(results))[0].answer;
					// Generate a query that will return three wrong answers back.
					var wrong_answers_query = "SELECT DISTINCT cname as choices \
												FROM Athlete inner join Nation using (noc) \
												WHERE noc <> '"+answer+"' \
												ORDER BY RAND() LIMIT 3;";
					connection.query(wrong_answers_query, function(err, results, fields) {
						if (err) {
							console.log(err);
							callback([]);
						} else {
							// Get the wrong answers back
							var json_choices = JSON.parse(JSON.stringify(results));
							var wrong_choices = [];
							// Put the wrong answer in a list and combine with right answer
							for (var i = 0; i < json_choices.length; i++) {
								wrong_choices[i] = json_choices[i].choices;
							}
							wrong_choices.push(answer);
							wrong_choices = shuffle(wrong_choices);
							// Return all the information back
							callback([question, wrong_choices, answer]);
						}
					});
				}
			});
		}
	});
}

// Question 2: who is the winner of a game in certain edition? 
function question2(callback) {
	// same as question one, with different question type
	// pick elements used for question
	var element_query = "SELECT DISTINCT edition as ele0, event as ele1, event_gender as ele2, discipline as ele3 \
						FROM Medal \
						ORDER BY RAND() \
						LIMIT 1;";
	connection.query(element_query, function(err, results, fields) {
		if (err) {
			console.log(err);
			callback([]);
		} else {
			// Get the result back
			var element = JSON.parse(JSON.stringify(results))[0];
			// Given the elements above, create a question.
			var adjusted = element.ele2;
			if (adjusted == "M"){
				adjusted = "Men";
			}
			if (adjusted == "W"){
				adjusted = "Women";
			}
			if (adjusted == "X"){
				adjusted = "Mixed";
			}
			if (typeof element.ele1 == "undefined") {
				console.log("question2 no element.");
				callback([]);
			}
			var eventTitle = toTitleCase(element.ele1);
			var disc = element.ele3;
			var question = "";
			if (disc == eventTitle){
				question = "Who is the winner of "+" "+adjusted+" "+eventTitle+" in year "+element.ele0+"? ";
			} else {
				question = "Who is the winner of "+" "+adjusted+" "+eventTitle+" "+element.ele3+" in year "+element.ele0+"? ";
			}
			
			// var question = "Who is the winner of "+" "+adjusted+" "+element.ele1+" in year "+element.ele0+"? "+element.ele3+"";
			console.log(err);
			// Generate a query that will return a right answer back.
			// This query should be used (connection.query...) in function generate_quiz.
			var query = "SELECT aname AS answer \
						FROM Medal \
						WHERE edition = '"+element.ele0+"' AND event = '"+element.ele1+"' AND \
						event_gender = '"+element.ele2+"' and mtype = 'Gold';";
			connection.query(query, function(err, results, fields) {
				if (err) {
					console.log(err);
					callback([]);
				} else {
					// Get the answer back
					var answer = JSON.parse(JSON.stringify(results))[0].answer;
					if (typeof answer == "undefined") {
						console.log("question2 no answer.");
						callback([]);
					}
					var answer1 = transformName(answer);
					// Generate a query to get three wrong choices back
					var wrong_answers_query = "SELECT DISTINCT aname AS choices \
												FROM Medal \
												WHERE aname <> '"+answer+"' and edition = '"+element.ele0+"' \
												AND discipline = (SELECT DISTINCT discipline FROM Medal \
																	WHERE aname = '"+answer+"' LIMIT 1) \
												ORDER BY RAND() LIMIT 3;";
					connection.query(wrong_answers_query, function(err, results, fields) {
						if (err) {
							console.log(err);
							callback([]);
						} else {
							// Get the wrong answers back
							var json_choices = JSON.parse(JSON.stringify(results));
							var wrong_choices = [];
							// Put the wrong answer in a list and combine with right answer
							for (var i = 0; i < json_choices.length; i++) {
								wrong_choices[i] = transformName(json_choices[i].choices);
							}
							wrong_choices.push(answer1);
							wrong_choices = shuffle(wrong_choices);
							// Return all the information back
							callback([question, wrong_choices, answer1]);
						}
					});
				}
			});
		}
	});
}

// Question 3: Which athlete below have won most gold medals?
function question3(callback) {
	// same as question one, with different question type
	var question = "Which athlete below have won most gold medals?";

	// Generate a query that will return a right answer back.
	// This query should be used (connection.query...) in function generate_quiz.
	var query = "SELECT aname AS answer \
				FROM Medal \
				WHERE mtype = 'Gold' \
				GROUP BY aname \
				HAVING count(*) >= 7 \
				ORDER BY RAND() LIMIT 1;";
	connection.query(query, function(err, results, fields) {
		if (err) {
			console.log(err);
			callback([]);
		} else {
			// Get the answer back
			var answer = JSON.parse(JSON.stringify(results))[0].answer;
			if (typeof answer == "undefined") {
				console.log("question3 no answer.");
				callback([]);
			}
			var answer1 = transformName(answer);
			// Generate a query to get three wrong choices back
			var wrong_answers_query = "SELECT DISTINCT aname AS choices FROM Athlete \
										WHERE aname NOT IN \
										(SELECT aname FROM Medal \
											WHERE mtype = 'Gold' \
											GROUP BY aname \
											HAVING count(*) > 7) \
										ORDER BY RAND() LIMIT 3;";
			connection.query(wrong_answers_query, function(err, results, fields) {
				if (err) {
					console.log(err);
					callback([]);
				} else {
					// Get the wrong answers back
					var json_choices = JSON.parse(JSON.stringify(results));
					var wrong_choices = [];
					// Put the wrong answer in a list and combine with right answer
					for (var i = 0; i < json_choices.length; i++) {
						wrong_choices[i] = transformName(json_choices[i].choices);
					}
					wrong_choices.push(answer1);
					wrong_choices = shuffle(wrong_choices);
					// Return all the information back
					callback([question, wrong_choices, answer1]);
				}
			});
		}
	});
}

// Question 4: What sport does this given athlete play ?
function question4(callback) {
	// same as question one, with different question type
	// pick elements used for question
	var element_query = "SELECT aname AS element \
						FROM Medal \
						WHERE mtype = 'Gold' \
						GROUP BY aname \
						HAVING count(*) >3 \
						ORDER BY RAND() LIMIT 1;";
	connection.query(element_query, function(err, results, fields) {
		if (err) {
			console.log(err);
			callback([]);
		} else {
			// Get the result
			var element = JSON.parse(JSON.stringify(results))[0].element;
			if (typeof element == "undefined") {
				console.log("question4 no element.");
				callback([]);
			}
			var element1 = transformName(element);
			// Given the elements above, create a question.
			var question = "What sport does "+element1+" play?";
			// This query should be used (connection.query...) in function generate_quiz.
			var query = "SELECT discipline AS answer \
						FROM Medal \
						WHERE aname = '"+element+"';";
			connection.query(query, function(err, results, fields) {
				if (err) {
					console.log(err);
					callback([]);
				} else {
					// Get the answer back
					var answer = JSON.parse(JSON.stringify(results))[0].answer;
					// Generate a query to get three wrong choices back
					var wrong_answers_query = "SELECT DISTINCT discipline AS choices \
										FROM Medal \
										WHERE discipline <> '"+answer+"' \
										ORDER BY RAND() LIMIT 3;";
					connection.query(wrong_answers_query, function(err, results, fields) {
						if (err)  {
							console.log(err);
							callback([]);
						} else {
							// Get the wrong answers back
							var json_choices = JSON.parse(JSON.stringify(results));
							var wrong_choices = [];
							// Put the wrong answer in a list and combine with right answer
							for (var i = 0; i < json_choices.length; i++) {
								wrong_choices[i] = json_choices[i].choices;
							}
							wrong_choices.push(answer);
							wrong_choices = shuffle(wrong_choices);
							// Return all the information back
							callback([question, wrong_choices, answer]);
						}
					});
				}
			});
		}
	});
}

// Question 5: Which city below has hosted the most Summer Olympic Games?
function question5(callback) {
	// same as question one, with different question type
	var question = "Which city below has hosted the most Summer Olympic Games?";

	// Generate a query that will return a right answer back.
	// This query should be used (connection.query...) in function generate_quiz.
	var query = "SELECT city AS answer \
				FROM (SELECT DISTINCT city, COUNT (DISTINCT edition) AS count \
						FROM Games \
						GROUP BY city \
						HAVING count > 1) as cityCount \
				ORDER BY RAND() LIMIT 1;";
				
	connection.query(query, function(err, results, fields) {
		if (err) {
			console.log(err);
			callback([]);
		} else {
			// Get the answer back
			var answer = JSON.parse(JSON.stringify(results))[0].answer;
			// Generate a query to get three wrong choices back
			var wrong_answers_query = "SELECT city AS choices FROM \
											(SELECT DISTINCT city, COUNT(DISTINCT edition) as count \
											FROM Games \
											GROUP BY city \
											HAVING count = 1) as cityCount \
										ORDER BY RAND() LIMIT 3;";
										
			connection.query(wrong_answers_query, function(err, results, fields) {
				if (err) {
					console.log(err);
					callback([]);
				} else {
					// Get the wrong answers back
					var json_choices = JSON.parse(JSON.stringify(results));
					var wrong_choices = [];
					// Put the wrong answer in a list and combine with right answer
					for (var i = 0; i < json_choices.length; i++) {
						wrong_choices[i] = json_choices[i].choices;
					}
					wrong_choices.push(answer);
					wrong_choices = shuffle(wrong_choices);
					// Return all the information back
					callback([question, wrong_choices, answer]);
				}
			});
		}
	});
}

// render and send the page to clients
function output_mywork(req, res, results) {
	if (!req.query.next) {
		res.render('quiz',
		   		{ questions: results[0],
		     		choices: results[1],
		     		user: req.user
			    }
	  	);
	} else {
		var choice_list = "[";
		for (var i = 0; i < results[1].length-1; i++) {
			choice_list += "{\"choice\":\"" + results[1][i] + "\"},";
		}
		choice_list += "{\"choice\":\"" + results[1][results[1].length-1] + "\"}]";
		var data = 	"{ \"questions\": \"" + results[0] + "\", \
		     			\"choices\": " + choice_list + "}  ";

		res.writeHead(200, {'Content-Type': 'application/json','Content-Length':data.length});
		res.write(data);
		res.end();
	}
}

// shuffle the array and generate a new unordered array
function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

function toTitleCase(str)
{
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}

function transformName(str) {
	str = str.split(",");
	if (str.length < 2) {
		return str;
	}
	var str1 = toTitleCase(str[0]);
	var str2 = toTitleCase(str[1]);
	return str2+" "+str1;
}
// Activated by app.js
exports.quiz = function(req, res) {
	if (!req.query.answer) {
		generate_quiz(req, res);
	}
	else {
		generate_answer(req, res);
	}
};