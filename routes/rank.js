var mysql = require('mysql');
var connection = mysql.createConnection({
  host     : 'dbolympic.cajidywjgjlk.us-east-1.rds.amazonaws.com',
  user     : 'guest1',
  password : '12345678',
  database : 'olym'
});

function query_db(req, res){
	var year = req.query.year;

	var query1 = "SELECT N.cname, COUNT(distinct M.event, M.event_gender, M.discipline, M.mtype) AS ct\
                 FROM Nation N, Nat_year NY, Medal M WHERE NY.noc = N.noc \
                 and M.edition = "+year+" and M.noc = NY.noc and NY.year = "+year+" \
                 GROUP BY N.cname \
                 ORDER BY ct DESC LIMIT 20;";
  var query2 = "select N.cname, count(distinct M.event, M.event_gender, M.discipline, M.mtype)/(NY.gdp/10000000000) AS ct\
  							 from Nation N, Nat_year NY, Medal M where NY.noc = N.noc and M.edition = "+year+" and M.noc = NY.noc and \
  							 NY.year = "+year+" group by N.cname order by ct desc LIMIT 20;";
  var query3 = "select N.cname, count(distinct M.event, M.event_gender, M.discipline, M.mtype)/(NY.pop/1000000) AS ct\
  							 from Nation N, Nat_year NY, Medal M where NY.noc = N.noc and M.edition = "+year+" and M.noc = NY.noc and \
  							 NY.year = "+year+" group by N.cname order by ct desc LIMIT 20;";
	connection.query(query1, function(err, rows, fields) {
		if (err) console.log(err);
		else {
			var r1 = rows;
			connection.query(query2, function(err, rows, fields) {
				if (err) console.log(err);
				else {
					var r2 = rows;
					connection.query(query3, function(err, rows, fields) {
						if (err) console.log(err);
						else {
							var r3 = rows;
							output_mywork(req, res, r1, r2, r3);
						}
					});
				}
			});
			// This is just for test, change to the above line after query is implemented
			// output_mywork(res, [nation, 'edition', 'discipline']);
		}
	});
}
function output_mywork(req, res, r1, r2, r3) {
	res.render('rank',
		   { title: 'Total Medal Counts Ranking for '+req.query.year+' Olympics Games',
		     results1: r1,
		     results2: r2,
		   	 results3: r3,
		   	 user: req.user
		   }
	  );
}
exports.show = function(req, res){
  query_db(req, res);
};