

exports.findById = function(id, cb) {
  process.nextTick(function() {
    var AWS = require("aws-sdk");
    AWS.config.update({
      region: "us-east-1",
      endpoint: "dynamodb.us-east-1.amazonaws.com"
    });
    var docClient = new AWS.DynamoDB.DocumentClient();
    var params = {
      TableName : "Users",
      KeyConditionExpression: "#id = :num",
      ExpressionAttributeNames:{
        "#id": "id"
      },
      ExpressionAttributeValues: {
        ":num":id
      }
    };
    docClient.query(params, function(err, data) {
      if (err) {
        console.error("Unable to query. Error:", JSON.stringify(err, null, 2));
      } else {
        console.log("Query succeeded.");
        if (data.Count) {
          cb(null, data.Items[0]);
        } else {
          cb(new Error('User ' + id + ' does not exist'));
        }
      }
    });
  });
};

exports.findByUsername = function(email, cb) {
  process.nextTick(function() {
    var AWS = require("aws-sdk");
    AWS.config.update({
      region: "us-east-1",
      endpoint: "dynamodb.us-east-1.amazonaws.com"
    });
    var docClient = new AWS.DynamoDB.DocumentClient();
    var params = {
      TableName : "Users",
      IndexName: "email-index",
      KeyConditionExpression: "#em = :name",
      ExpressionAttributeNames:{
        "#em": "email"
      },
      ExpressionAttributeValues: {
        ":name": email
      }
    };
    docClient.query(params, function(err, data) {
      if (err) {
        console.error("Unable to query. Error:", JSON.stringify(err, null, 2));
      } else {
        console.log("Query succeeded.");
        if (data.Count) {
          return cb(null, data.Items[0]);
        } else {
          return cb(null, null);
        }
      }
    });
  });
};

exports.signUp = function(req, res) {
    var bcrypt = require('bcrypt');
    const saltRounds = 10;
    var AWS = require("aws-sdk");
    bcrypt.hash(req.body.password, saltRounds, function(err, hash) {
      if (err) { console.error("Unable to hash password. Error JSON:", JSON.stringify(err, null, 2)); }
    AWS.config.update({
      region: "us-east-1",
      endpoint: "dynamodb.us-east-1.amazonaws.com"
    });
    var docClient = new AWS.DynamoDB.DocumentClient();
    var scanParams = {
      TableName : "Users",
      ProjectionExpression: "#id",
      ExpressionAttributeNames: { "#id": "id", }
    };
    docClient.scan(scanParams, function(err, data) {
    var maxId = 0;
    if (err) {
        console.error("Unable to scan the table. Error JSON:", JSON.stringify(err, null, 2));
    } else if (data.Items[0]) {
        data.Items.forEach(function(item) {
        if (maxId < item.id) {
          maxId = item.id;
        }
      });
    }
    var params = {
      TableName : "Users",
      Item: {
        "id": maxId + 1,
        "email": req.body.email,
        "password": hash,
        "first_name": req.body.firstName,
        "last_name": req.body.lastName,
        "gender": req.body.gender,
        "scores": []
      }
    };
    docClient.put(params, function(err, data) {
      if (err) {
        console.error("Error JSON:", JSON.stringify(err, null, 2));
      } else {
        console.log("PutItem succeeded");
      }
    res.redirect('/login');
  });
    });
});
};

exports.uploadScore = function(req, res) {
    if (req.user) {
    console.log(req.body);
    var AWS = require("aws-sdk");
    AWS.config.update({
      region: "us-east-1",
      endpoint: "dynamodb.us-east-1.amazonaws.com"
    });
    var score = [[req.body.score, String(Date())]];
    var docClient = new AWS.DynamoDB.DocumentClient();
    var params = {
      TableName : "Users",
      Key:{
        "id": req.user.id
      },
      UpdateExpression: "set scores = list_append(scores, :i)",
      ExpressionAttributeValues:{
        ":i": score
      },
      ReturnValues:"UPDATED_NEW"
    };
    docClient.update(params, function(err, data) {
    if (err) {
        console.error("Unable to update item. Error JSON:", JSON.stringify(err, null, 2));
    } else {
        console.log("UpdateItem succeeded:", JSON.stringify(data, null, 2));
    }
    });
    res.sendStatus(200);
    } else {
      res.sendStatus(200);
    }
};